#ifndef MINAL_INCLUDE_VEC_HPP
#define MINAL_INCLUDE_VEC_HPP

#include <cmath>
#include <cstddef>

#define two_pi 6.2831853071f
#define pi     3.1415926535f

#define COUT_V2(X) "(" << (X).x << ", " << (X).y << ")"
#define COUT_V3(X) "(" << (X).x << ", " << (X).y << ", " << (X).z << ")"

struct v2
{
    float x, y;
    float* data() { return &x; }
    float& operator[](int idx) { return *(&x + idx); }
    float& operator[](size_t idx) { return *(&x + idx); }
};

struct v3
{
    float x, y, z;
    float* data() { return &x; }
    float& operator[](int idx) { return *(&x + idx); }
    float& operator[](size_t idx) { return *(&x + idx); }
};

struct v4
{
    float x, y, z, w;
    float* data() { return &x; }
    float& operator[](int idx) { return *(&x + idx); }
    float& operator[](size_t idx) { return *(&x + idx); }
};

struct m2
{
    v2 x, y;
    float* data() { return &(x.x); }
    v2& operator[](int idx) { return *(&x + idx); }
    v2& operator[](size_t idx) { return *(&x + idx); }
};

struct m3
{
    v3 x, y, z;
    float* data() { return &(x.x); }
    v3& operator[](int idx) { return *(&x + idx); }
    v3& operator[](size_t idx) { return *(&x + idx); }
};

struct m4
{
    v4 x, y, z, w;
    float* data() { return &(x.x); }
    v4& operator[](int idx) { return *(&x + idx); }
    v4& operator[](size_t idx) { return *(&x + idx); }
};

inline v2 operator+ (v2 a, v2 b)
{
    return {a.x + b.x, a.y + b.y};
}

inline v3 operator+ (v3 a, v3 b)
{
    return {a.x + b.x, a.y + b.y, a.z + b.z};
}

inline v4 operator+ (v4 a, v4 b)
{
    return {a.x + b.x, a.y + b.y, a.z + b.z, a.w + b.w};
}

inline v2 operator- (v2 a, v2 b)
{
    return {a.x - b.x, a.y - b.y};
}

inline v3 operator- (v3 a, v3 b)
{
    return {a.x - b.x, a.y - b.y, a.z - b.z};
}

inline v2 operator* (v2 v, float k)
{
    return {v.x * k, v.y * k};
}

inline v2 operator* (float k, v2 v)
{
    return {v.x * k, v.y * k};
}

inline v3 operator* (v3 v, float k)
{
    return {v.x * k, v.y * k, v.z * k};
}

inline v3 operator* (float k, v3 v)
{
    return {v.x * k, v.y * k, v.z * k};
}

inline v4 operator* (v4 v, float k)
{
    return {v.x * k, v.y * k, v.z * k, v.w * k};
}

inline v4 operator* (float k, v4 v)
{
    return {v.x * k, v.y * k, v.z * k, v.w * k};
}

inline v4 operator* (v4 a, v4 b)
{
    return {a.x * b.x, a.y * b.y, a.z * b.z, a.w * b.w};
}

inline v2 operator/ (v2 v, float k)
{
    return {v.x / k, v.y / k};
}

inline v2& operator+=(v2& v, v2 o)
{
    v.x += o.x; v.y += o.y;
    return v;
}

inline v3& operator+=(v3& v, v3 o)
{
    v.x += o.x; v.y += o.y; v.z += o.z;
    return v;
}

inline v4& operator-=(v4& v, v4 o)
{
    v.x -= o.x; v.y -= o.y; v.z -= o.z; v.w -= o.w;
    return v;
}

inline v2& operator*=(v2& v, float k)
{
    v.x *= k; v.y *= k;
    return v;
}

inline v3& operator*=(v3& v, float k)
{
    v.x *= k; v.y *= k; v.z *= k;
    return v;
}

inline v4& operator*=(v4& v, float k)
{
    v.x *= k; v.y *= k; v.z *= k; v.w *= k;
    return v;
}

inline v4& operator*=(v4& a, v4 b)
{
    a.x *= b.x;
    a.y *= b.y;
    a.z *= b.z;
    a.w *= b.w;
    return a;
}

inline v2& operator/= (v2& v, float k)
{
    v.x /= k; v.y /= k;
    return v;
}

inline v3& operator/= (v3& v, float d)
{
    v.x /= d; v.y /= d; v.z /= d;
    return v;
}

inline v2 operator* (m2 m, v2 v)
{
    return {m.x.x * v.x + m.y.x * v.y, m.x.y * v.x + m.y.y * v.y};
}

inline float minf (float a, float b)
{
    if (a < b) return a; else return b;
}

inline float maxf (float a, float b)
{
    if (a < b) return b; else return a;
}

inline double mind(double a, double b)
{
    if (a < b) return a; else return b;
}

inline double maxd(double a, double b)
{
    if (a < b) return b; else return a;
}

inline int mini(int a, int b)
{
    if (a < b) return a; else return b;
}

inline int maxi(int a, int b)
{
    if (a < b) return b; else return a;
}

inline size_t minu(size_t a, size_t b)
{
    if (a < b) return a; else return b;
}

inline size_t maxu(size_t a, size_t b)
{
    if (a < b) return b; else return a;
}

inline float dot (v2 a, v2 b)
{
    return a.x * b.x + a.y * b.y;
}

inline float dot (v3 a, v3 b)
{
    return a.x * b.x + a.y * b.y + a.z * b.z;
}

inline float dot (v4 a, v4 b)
{
    return a.x * b.x + a.y * b.y + a.z * b.z + a.w * b.w;
}

inline float mix(float a, float b, float t)
{
    return a * (1.0f - t) + b * t;
}

inline float mix(float a, float b, float c, v3 t)
{
    return a * t.x + b * t.y + c * t.z;
}

inline v2 mix(v2 a, v2 b, v2 c, v3 t)
{
    v2 v;
    v.x = a.x * t.x + b.x * t.y + c.x * t.z;
    v.y = a.y * t.x + b.y * t.y + c.y * t.z;
    return v;
}

inline v3 mix (v3 a, v3 b, v3 c, v3 t)
{
    v3 v;
    v.x = a.x * t.x + b.x * t.y + c.x * t.z;
    v.y = a.y * t.x + b.y * t.y + c.y * t.z;
    v.z = a.z * t.x + b.z * t.y + c.z * t.z;
    return v;
}

inline v2 abs (v2 v)
{
    if (v.x < 0) v.x = -v.x;
    if (v.y < 0) v.y = -v.y;
    return v;
}

inline float clamp (float val, float mini, float maxi)
{
    if (val < mini) val = mini;
    if (val > maxi) val = maxi;
    return val;
}

inline v2 clamp (v2 v, float mini, float maxi)
{
    return {clamp(v.x, mini, maxi), clamp(v.y, mini, maxi)};
}

inline float smootherstep (float edge0, float edge1, float x)
{
  // Scale, and clamp x to 0..1 range
  x = clamp((x - edge0) / (edge1 - edge0), 0.0, 1.0);
  // Evaluate polynomial
  return x * x * x * (x * (x * 6 - 15) + 10);
}

inline v3 barycentric(v3 v)
{
    v3 b;
    float sum = v.x + v.y + v.z;
    v3 n = {1.0f, 1.0f, 1.0f};
    b = v + (1.0f - sum) / 3.0f * n;
    return b;
}

inline v4 barycentric(v4 v)
{
    v4 b;
    float sum = v.x + v.y + v.z + v.w;
    v4 n = {0.25f, 0.25f, 0.25f, 0.25f};
    b = v + (1.0f - sum) * n;
    return b;
}

inline v2 poly(float ka, v2 a, float kb, v2 b)
{
    return {ka * a.x + kb * b.x, ka * a.y + kb * b.y};
}

inline float fract(float x)
{
    return x - std::floor(x);
}

inline v2 fract (v2 v)
{
    return {fract(v.x), fract(v.y)};
}

inline float length (v2 v)
{
    return std::sqrt(v.x * v.x + v.y * v.y);
}

inline float length (v3 v)
{
    return std::sqrt(v.x * v.x + v.y * v.y + v.z * v.z);
}

inline float length (v4 v)
{
    return std::sqrt(v.x * v.x + v.y * v.y + v.z * v.z + v.w * v.w);
}

inline v3 normalize (v3 v)
{
    float m = length(v);
    return {v.x / m, v.y / m, v.z / m};
}

inline v4 normalize (v4 v)
{
    float m = length(v);
    return {v.x / m, v.y / m, v.z / m, v.w / m};
}

inline v3 to_sum_1 (v3 v)
{
    float s = v.x + v.y + v.z;
    return {v.x / s, v.y / s, v.z / s};
}

inline v3 cross (v3 a, v3 b)
{
    return {
        a.y * b.z - a.z * b.y,
        a.z * b.x - a.x * b.z,
        a.x * b.y - a.y * b.x
    };
}

// returns k * a + b
inline v3 fma (float k, v3 a, v3 b)
{
    v3 result;
    result.x = std::fma(k, a.x, b.x);
    result.y = std::fma(k, a.y, b.y);
    result.z = std::fma(k, a.z, b.z);
    return result;
}

// transforms to [-1:1] x [-1:1] x [-1:1]
inline m4 ortho(float width, float height, float near, float far)
{
    return {
        {2.0f / width, 0.0f, 0.0f, 0.0f},
        {0.0f, 2.0f / height, 0.0f, 0.0f},
        {0.0f, 0.0f, -2.0f / (far - near), 0.0f},
        {0.0f, 0.0f, - (far + near) / (far - near), 1.0f}
    };
}

// forward: -z, up: y, right: x
inline m4 lookat(v3 from, v3 to, v3 up)
{
    v3 forward = normalize(to - from);
    v3 right = cross(forward, up);
    v3 fixed_up = cross(right, forward);
    return {
        {right.x, fixed_up.x, -forward.x, 0},
        {right.y, fixed_up.y, -forward.y, 0},
        {right.z, fixed_up.z, -forward.z, 0},
        {-dot(right, from), -dot(fixed_up, from), dot(forward, from), 1}
    };
}


// forward: -z, up: y, right: x
inline m4 view(v3 pos, v3 forward, v3 up)
{
    v3 right = cross(forward, up);
    v3 fixed_up = cross(right, forward);
    return {
        {right.x, fixed_up.x, -forward.x, 0},
        {right.y, fixed_up.y, -forward.y, 0},
        {right.z, fixed_up.z, -forward.z, 0},
        {-dot(right, pos), -dot(fixed_up, pos), dot(forward, pos), 1}
    };
}

inline m2 rotation(float rad)
{
    float c = std::cos(rad);
    float s = std::sin(rad);
    return {
        {c, s},
        {-s, c}
    };
}

// x: [0:1], center: [0:1]
inline float triangle_distribution(float x, float center)
{
    // https://en.wikipedia.org/wiki/Triangular_distribution
    // a = 0, b = 1
    if (x < center) return std::sqrt(center * x);
    else return 1.0f - std::sqrt( (1.0f - center) * (1.0f - x) );
}

#endif
