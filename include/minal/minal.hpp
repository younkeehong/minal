#ifndef MINAL_INCLUDE_MINAL_HPP
#define MINAL_INCLUDE_MINAL_HPP

// opengl functions
#ifdef _WIN32
#ifndef APIENTRY
#define APIENTRY __stdcall
#endif
#endif

#include <glad/glad.h>

#ifdef APIENTRY
#undef APIENTRY
#endif

// glfw forward declarations (only for client code)
#ifndef MINAL_INTERNAL
extern "C" {

#define GLFW_TRUE                   1
#define GLFW_FALSE                  0

#define GLFW_RELEASE                0
#define GLFW_PRESS                  1
#define GLFW_REPEAT                 2

#define GLFW_MOD_SHIFT           0x0001
#define GLFW_MOD_CONTROL         0x0002
#define GLFW_MOD_ALT             0x0004

#define GLFW_KEY_SPACE              32
#define GLFW_KEY_APOSTROPHE         39  /* ' */
#define GLFW_KEY_COMMA              44  /* , */
#define GLFW_KEY_MINUS              45  /* - */
#define GLFW_KEY_PERIOD             46  /* . */
#define GLFW_KEY_SLASH              47  /* / */

#define GLFW_KEY_0                  48
#define GLFW_KEY_1                  49
#define GLFW_KEY_2                  50
#define GLFW_KEY_3                  51
#define GLFW_KEY_4                  52
#define GLFW_KEY_5                  53
#define GLFW_KEY_6                  54
#define GLFW_KEY_7                  55
#define GLFW_KEY_8                  56
#define GLFW_KEY_9                  57

#define GLFW_KEY_SEMICOLON          59  /* ; */
#define GLFW_KEY_EQUAL              61  /* = */

#define GLFW_KEY_A                  65
#define GLFW_KEY_B                  66
#define GLFW_KEY_C                  67
#define GLFW_KEY_D                  68
#define GLFW_KEY_E                  69
#define GLFW_KEY_F                  70
#define GLFW_KEY_G                  71
#define GLFW_KEY_H                  72
#define GLFW_KEY_I                  73
#define GLFW_KEY_J                  74
#define GLFW_KEY_K                  75
#define GLFW_KEY_L                  76
#define GLFW_KEY_M                  77
#define GLFW_KEY_N                  78
#define GLFW_KEY_O                  79
#define GLFW_KEY_P                  80
#define GLFW_KEY_Q                  81
#define GLFW_KEY_R                  82
#define GLFW_KEY_S                  83
#define GLFW_KEY_T                  84
#define GLFW_KEY_U                  85
#define GLFW_KEY_V                  86
#define GLFW_KEY_W                  87
#define GLFW_KEY_X                  88
#define GLFW_KEY_Y                  89
#define GLFW_KEY_Z                  90

#define GLFW_KEY_LEFT_BRACKET       91  /* [ */
#define GLFW_KEY_BACKSLASH          92  /* \ */
#define GLFW_KEY_RIGHT_BRACKET      93  /* ] */
#define GLFW_KEY_GRAVE_ACCENT       96  /* ` */

#define GLFW_KEY_ESCAPE             256
#define GLFW_KEY_ENTER              257
#define GLFW_KEY_TAB                258
#define GLFW_KEY_BACKSPACE          259
#define GLFW_KEY_INSERT             260
#define GLFW_KEY_DELETE             261
#define GLFW_KEY_RIGHT              262
#define GLFW_KEY_LEFT               263
#define GLFW_KEY_DOWN               264
#define GLFW_KEY_UP                 265

#define GLFW_MOUSE_BUTTON_1         0
#define GLFW_MOUSE_BUTTON_2         1
#define GLFW_MOUSE_BUTTON_3         2

#define GLFW_MOUSE_BUTTON_LEFT      GLFW_MOUSE_BUTTON_1
#define GLFW_MOUSE_BUTTON_RIGHT     GLFW_MOUSE_BUTTON_2
#define GLFW_MOUSE_BUTTON_MIDDLE    GLFW_MOUSE_BUTTON_3

struct GLFWwindow;
typedef void (* GLFWkeyfun)(GLFWwindow*,int,int,int,int);
typedef void (* GLFWmousebuttonfun)(GLFWwindow*,int,int,int);

}
#endif

// rtaudio forward declarations (only for client code)
#ifndef MINAL_INTERNAL
class RtAudio;
using RtAudioCallback = int (*) (void* out, void* in, unsigned num_frames,
                                 double time, unsigned status, void *data);
#define RTAUDIO_INPUT_OVERFLOW 1
#define RTAUDIO_OUTPUT_UNDERFLOW 2
#endif

#ifndef MINAL_INTERNAL
namespace oscpkt
{
    struct UdpSocket;
    struct PacketWriter;
    struct Message;
}
#endif

#include <cstddef> // size_t, ptrdiff_t, offsetof
#include <cstdint>
#include <string>
#include <vector>
#include <thread>
#include <atomic>
#include <iostream>

#define VAO_ATTR_NUM 8
#define VAO_BUFFER_NUM 8
#define FBO_COLOR_NUM 8

std::string file_to_string (const char* name);
void print_if (const char* label, const char* err);

struct window
{
    GLFWwindow* glfw_win = nullptr;
    int width = 0, height = 0;
    int framebuffer_width = 0, framebuffer_height = 0;
    int windowed_width = 0, windowed_height = 0;

    window () = default;
    window (const window&) = delete;
    window (window&&) = delete;
    window& operator= (const window&) = delete;
    window& operator= (window&&) = delete;
    ~window (); // will close window if present and terminate glfw
};

int print_monitors ();
void open_fullscreen (window*);
void open_fullscreen (window*, int idx);
void open_window (window*, int width, int height);
void open_window_ms (window*, int width, int height, int samples=4);
void close_window (window*);
void go_fullscreen (window*);
void end_fullscreen (window*);
bool is_fullscreen (GLFWwindow*);
void update_size (window*);

bool should_close (window* io);
void swap_buffers (window* io);
void poll_events ();

void set_key_callback (window*, GLFWkeyfun func);
void set_mouse_callback (window*, GLFWmousebuttonfun func);
void set_user_pointer (window*, void* data);
void* get_user_pointer (GLFWwindow* window);
bool is_mod_key (int key);
// top-left is (0, 0)
void cursor_pos (window*, float* x, float* y);

void set_should_close (GLFWwindow*);
inline void set_should_close (window* io) { set_should_close(io->glfw_win); }

struct audio
{
    RtAudio* rtaudio = nullptr;
    // these are all unsigned in RtAudio definitions
    unsigned channels = 0;
    unsigned num_buffers = 0;
    unsigned sampling_rate = 0;
    unsigned device_id = 0;
    unsigned buffer_size = 0;

    audio () = default;
    audio (const audio&) = delete;
    audio (audio&&) = delete;
    audio& operator= (const audio&) = delete;
    audio& operator= (audio&&) = delete;
    ~audio (); // will stop/close stream if running/open and free rtaudio
};

void open_audio_stream (audio*, RtAudioCallback, void*);
bool is_audio_stream_open (audio*);
void start_audio_stream (audio*);
void close_audio_stream (audio*);

struct shader
{
    GLuint program = 0;
};

struct uniform
{
    GLint loc = -1;
};

shader create_shader (const char* vert, const char* frag);
void delete_shader (shader*);

struct vao
{
    GLuint va = 0;
    GLuint buffer[VAO_BUFFER_NUM] = {};
    GLuint indices = 0;
};

struct attr_info
{
    int buffer = -1;
    GLenum type = GL_FLOAT;
    int dimension = 0;
    int stride = 0;   // in bytes
    int offset = 0; // in bytes
    bool normalize = false;
    bool per_instance = false;
};

struct vao_info
{
    attr_info attr[VAO_ATTR_NUM];
    bool use_indices = false;
};

vao create_vao (vao_info);
void delete_vao (vao* v);

struct texture
{
    GLuint tex = 0;
};

struct tex_info
{
    GLint format = GL_RGBA8;
    GLint filter = GL_NEAREST;
    GLint wrap = GL_CLAMP_TO_EDGE;
    const GLvoid* data = nullptr;
    GLenum data_type = GL_UNSIGNED_BYTE;
    GLenum data_format = GL_RGBA;
};

texture create_texture (int width, int height, tex_info);
inline texture create_texture (int width, int height)
{
    tex_info ti;
    return create_texture(width, height, ti);
}
void delete_texture (texture*);

struct texture_ms
{
    GLuint tex = 0;
    GLsizei samples = 0;
};

struct tex_ms_info
{
    GLint format = GL_RGBA8;
    GLsizei samples = 4;
};

texture_ms create_texture_ms (int width, int height, tex_ms_info);
inline texture_ms create_texture_ms (int width, int height)
{
    tex_ms_info ti;
    return create_texture_ms(width, height, ti);
}
void delete_texture_ms (texture_ms*);

struct fbo
{
    GLuint fb = 0;
    GLuint depth = 0; // non-zero if using renderbuffer
    int width, height;
};

struct fbo_info
{
    int width = 0, height = 0;
    texture color[FBO_COLOR_NUM];
    texture depth; // if not assigned depth.tex is 0 and in that case
                   // renderbuffer will be created and be used
};

fbo create_fbo (fbo_info);
void delete_fbo (fbo*);

struct fbo_ms
{
    GLuint fb = 0;
    GLuint depth = 0; // non-zero if using renderbuffer
    int width, height;
};

struct fbo_ms_info
{
    int width = 0, height = 0;
    texture_ms color[FBO_COLOR_NUM];
    texture_ms depth; // if not assigned depth.tex is 0 and in that case
                   // renderbuffer will be created and be used
};

fbo_ms create_fbo_ms (fbo_ms_info);
void delete_fbo_ms (fbo_ms*);

struct shader_manager
{
    std::vector<shader> shaders;
};

shader create_shader (shader_manager* m, const char* vert, const char* frag);
void delete_shaders (shader_manager* m);

struct texture_manager
{
    std::vector<texture> textures;
};

texture create_texture (texture_manager* m, int width, int height, tex_info ti);
texture create_texture (texture_manager* m, int width, int height);
void delete_textures (texture_manager* m);

struct fbo_manager
{
    std::vector<fbo> fbos;
};

fbo create_fbo (fbo_manager* m, fbo_info fi);
void delete_fbos (fbo_manager* m);

struct vao_manager
{
    std::vector<vao> vaos;
};

vao create_vao (vao_manager* m, vao_info fi);
void delete_vaos (vao_manager* m);

void init_imgui (window*);
void imgui_begin_frame ();
void imgui_end_frame ();

void begin_inspector ();
void end_inspector ();

void begin_control ();
void end_control ();

void imgui_draw ();
void quit_imgui ();
bool using_gui ();

bool button (const char* label);
bool toggle (const char* label, bool* val);
bool sliderf (const char* label, float* val, float mini = 0.0f, float maxi = 1.0f);
bool slideri (const char* label, int* val, int minimum, int maximum);

void gui_image (GLuint id, float width, float height);
void gui_text (const char* fmt, ...);

void hline ();
void newline ();
void spacing ();
void same_line ();
void gui_item_width (float);

bool radio (const char* label, int* idx, int val);
bool radioh (const char** labels, int num_options, int* val);
bool radiov (const char** labels, int num_options, int* val);
bool combo (const char* label, int* current_item, const char** items, int items_count);

float get_fps (); // needs imgui_begin_frame called every frame to be updated

struct osc_server
{
    std::atomic<bool> quit_flag {false};
    std::thread thread_handle;

    osc_server () = default;
    osc_server (const osc_server&) = delete;
    osc_server (osc_server&&) = delete;
    osc_server& operator= (const osc_server&) = delete;
    osc_server& operator= (osc_server&&) = delete;
    ~osc_server () { quit_flag.store(true); if (thread_handle.joinable()) thread_handle.join(); }
};

struct osc_recv_msg;
using osc_recv_callback_t = void (*) (osc_recv_msg*, void*);

void start_osc_server (osc_server*, int port, osc_recv_callback_t, void* shared);
void quit_osc_server (osc_server*);
const char* get_addr (osc_recv_msg*);
bool match_pattern (osc_recv_msg*, const char* pattern);
int32_t pop_i32 (osc_recv_msg*);
float pop_f32 (osc_recv_msg*);

struct osc_sender
{
    oscpkt::UdpSocket* socket;
    oscpkt::PacketWriter* writer;

    osc_sender ();
    osc_sender (const osc_sender&) = delete;
    osc_sender (osc_sender&&) = delete;
    osc_sender& operator= (const osc_sender&) = delete;
    osc_sender& operator= (osc_sender&&) = delete;
    ~osc_sender ();
};

// allocates in constructor, reuse if performance wanted
struct osc_send_msg
{
    oscpkt::Message* msg;

    osc_send_msg ();
    osc_send_msg (const osc_send_msg&) = delete;
    osc_send_msg (osc_send_msg&&) = delete;
    osc_send_msg& operator= (const osc_send_msg&) = delete;
    osc_send_msg& operator= (osc_send_msg&&) = delete;
    ~osc_send_msg ();
};

void bind_sender (osc_sender*, const char* addr, int port);
void reset_msg (osc_send_msg*, const char* addr);
void push_i32 (osc_send_msg*, int32_t);
void push_f32 (osc_send_msg*, float);
bool send (osc_sender*, osc_send_msg*); // returns false if failed
bool send (osc_sender*, void* data, size_t size);

struct toml_impl;
struct toml_io
{
    toml_impl* impl;

    toml_io ();
    toml_io (const toml_io&);
    toml_io (toml_io&&) noexcept;
    toml_io& operator= (const toml_io&);
    toml_io& operator= (toml_io&&) noexcept;
    ~toml_io ();
};

toml_io parse_toml (const char* name);
double getf64 (toml_io*, const char* key);
int64_t geti64 (toml_io*, const char* key);
std::string gets(toml_io*, const char* key);
bool getb (toml_io*, const char* key);

struct json_impl;
struct json_io
{
    json_impl* impl;

    json_io ();
    json_io (const json_io&);
    json_io (json_io&&) noexcept;
    json_io& operator= (const json_io&);
    json_io& operator= (json_io&&) noexcept;
    ~json_io ();
};

json_io parse_json (const char* name);
double getf64 (json_io* j, const char* key);
int64_t geti64 (json_io* j, const char* key);
std::string gets (json_io* j, const char* key);
bool getb (json_io* j, const char* key);

struct image
{
    uint8_t* data = nullptr;
    int width = 0, height = 0;

    image () {}
    image (int w, int h);
    image (const image& other);
    image (image&& other) noexcept;
    image& operator= (const image& other);
    image& operator= (image&& other) noexcept;
    ~image ();
};

// returns empty object (width and height 0, data nullptr) if failed to load
image load_image (const char* filename);
void save_image (const char* filename, int w, int h, uint8_t* data);
inline void save_image (const char* filename, image* img)
{
    save_image(filename, img->width, img->height, img->data);
}

struct sound_sample
{
    // types are specified the backend drwav library
    unsigned channels = 0;
    unsigned sample_rate = 0;
    uint64_t frame_count = 0;
    float* samples = nullptr;

    sound_sample () = default;
    sound_sample (const sound_sample&);
    sound_sample (sound_sample&&) noexcept;
    sound_sample& operator= (const sound_sample&);
    sound_sample& operator= (sound_sample&&) noexcept;
    ~sound_sample ();
};

sound_sample load_wav (const char* filename); // check wavfile.sample != nullptr after
// TODO save_wav

void sleep_thread (int ms);

struct pcg_state
{
    uint64_t state;
    uint64_t inc;
};

pcg_state pcg_seed(uint64_t initstate, uint64_t initseq);
uint32_t pcg_run(pcg_state* pcg);
float rand01(pcg_state* pcg);

// inlined opengl functions

inline void use_program (shader s) { glUseProgram(s.program); }
inline void bind_vao (vao v) { glBindVertexArray(v.va); }
inline void bind_idx (vao v) { glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, v.indices); }

inline void buffer_data (vao v, int idx, int size, const GLvoid* data)
{
    glBindBuffer(GL_ARRAY_BUFFER, v.buffer[idx]);
    glBufferData(GL_ARRAY_BUFFER, size, data, GL_DYNAMIC_DRAW);
}
inline void index_data (vao v, int size, const GLvoid* data)
{
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, v.indices);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, size, data, GL_DYNAMIC_DRAW);
}

inline void clear_color (float* color) { glClearBufferfv(GL_COLOR, 0, color); }
inline void clear_depth (float depth) { glClearBufferfv(GL_DEPTH, 0, &depth); }
inline void draw_triangles (int count) { glDrawArrays(GL_TRIANGLES, 0, count); }
inline void draw_triangle_strip (int count) { glDrawArrays(GL_TRIANGLE_STRIP, 0, count); }
inline void draw_lines (int count) { glDrawArrays(GL_LINES, 0, count); }
inline void draw_lines (int first, int count) { glDrawArrays(GL_LINES, first, count); }
inline void draw_line_strip (int count) { glDrawArrays(GL_LINE_STRIP, 0, count); }
inline void draw_triangles_idx (int count) { glDrawElements(GL_TRIANGLES, count, GL_UNSIGNED_INT, 0); }
inline void draw_triangles_idx_instance (int count, int num_instance)
{
    glDrawElementsInstanced(GL_TRIANGLES, count, GL_UNSIGNED_INT, 0, num_instance);
}

inline void bind_fbo (fbo f)
{
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, f.fb);
    glViewport(0, 0, f.width, f.height);
}

inline void bind_fbo (GLuint f)
{
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, f);
}

inline void default_framebuffer (window* w) {
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
    glViewport(0, 0, w->framebuffer_width, w->framebuffer_height);
}

inline void bind_texture2d (texture t, int unit)
{
    glActiveTexture(GL_TEXTURE0 + unit);
    glBindTexture(GL_TEXTURE_2D, t.tex);
}
inline void unbind_texture2d (int unit)
{
    glActiveTexture(GL_TEXTURE0 + unit);
    glBindTexture(GL_TEXTURE_2D, 0);
}

inline uniform uniform_loc (shader s, const GLchar* name)
{
    uniform u;
    u.loc = glGetUniformLocation(s.program, name);
    return u;
}
inline void uniform1i (uniform u, GLint val) { glUniform1i(u.loc, val); }
inline void uniform1f (uniform u, GLfloat f) { glUniform1f(u.loc, f); }
inline void uniform2f (uniform u, GLfloat f0, GLfloat f1) { glUniform2f(u.loc, f0, f1); }
inline void uniform3f (uniform u, GLfloat* f) { glUniform3fv(u.loc, 1, f); }
inline void uniform4f (uniform u, GLfloat f0, GLfloat f1, GLfloat f2, GLfloat f3) { glUniform4f(u.loc, f0, f1, f2, f3); }

inline const char* gl_error_string ()
{
    GLenum e = glGetError();
    switch (e) {
        case GL_INVALID_ENUM:                  return "GL_INVALID_ENUM";
        case GL_INVALID_VALUE:                 return "GL_INVALID_VALUE";
        case GL_INVALID_OPERATION:             return "GL_INVALID_OPERATION";
        case GL_INVALID_FRAMEBUFFER_OPERATION: return "GL_INVALID_FRAMEBUFFER_OPERATION";
        case GL_OUT_OF_MEMORY:                 return "GL_OUT_OF_MEMORY";
        case GL_NO_ERROR:                      return "";
        default:                               return "UNKOWN CODE";
    }
}

#define GL_ERROR(X) { const char* err = gl_error_string(); if (err && err[0]) std::cout << X << ": " << err << std::endl; }

inline void depth_test (bool b) { if (b) glEnable(GL_DEPTH_TEST); else glDisable(GL_DEPTH_TEST); }
inline void blending (bool b) { if (b) glEnable(GL_BLEND); else glDisable(GL_BLEND); }
inline void depth_mask (bool b) { glDepthMask(b); }
// GL_FUNC_ADD, GL_FUNC_SUBTRACT, GL_FUNC_REVERSE_SUBTRACT, GL_MIN, GL_MAX
inline void blend_eq (GLenum eq) { glBlendEquation(eq); }
// GL_FUNC_ADD, GL_FUNC_SUBTRACT, GL_FUNC_REVERSE_SUBTRACT, GL_MIN, GL_MAX
inline void blend_eq (GLenum eqrgb, GLenum eqalpha) { glBlendEquationSeparate(eqrgb, eqalpha); }
// GL_ZERO, GL_ONE, GL_SRC_COLOR, GL_ONE_MINUS_SRC_COLOR, GL_DST_COLOR, GL_ONE_MINUS_DST_COLOR,
// GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA, GL_DST_ALPHA, GL_ONE_MINUS_DST_ALPHA. GL_CONSTANT_COLOR,
// GL_ONE_MINUS_CONSTANT_COLOR, GL_CONSTANT_ALPHA, and GL_ONE_MINUS_CONSTANT_ALPHA.
inline void blend_func (GLenum src, GLenum dst) { glBlendFunc(src, dst); }
// GL_ZERO, GL_ONE, GL_SRC_COLOR, GL_ONE_MINUS_SRC_COLOR, GL_DST_COLOR, GL_ONE_MINUS_DST_COLOR,
// GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA, GL_DST_ALPHA, GL_ONE_MINUS_DST_ALPHA. GL_CONSTANT_COLOR,
// GL_ONE_MINUS_CONSTANT_COLOR, GL_CONSTANT_ALPHA, and GL_ONE_MINUS_CONSTANT_ALPHA.
inline void blend_func (GLenum srcrgb, GLenum dstrgb, GLenum srcalpha, GLenum dstalpha)
{
    glBlendFuncSeparate(srcrgb, dstrgb, srcalpha, dstalpha);
}
inline void blending_additive ()
{
    blend_eq(GL_FUNC_ADD);
    blend_func(GL_SRC_ALPHA, GL_ONE);
}
inline void blending_alpha ()
{
    blend_eq(GL_FUNC_ADD);
    blend_func(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}

#endif

