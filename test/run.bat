@echo off
PUSHD .
cd build
cd ninja
cd release
ninja -j 2
POPD
if not errorlevel 1 (
    minal_test.exe
)
