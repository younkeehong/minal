#!/bin/bash

(
mkdir -p build
cd build
mkdir -p ninja
cd ninja
mkdir -p release
cd release

cmake -G "Ninja" -DCMAKE_BUILD_TYPE=Release ../../..
)
