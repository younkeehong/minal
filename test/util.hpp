#include <cstddef> // size_t, ptrdiff_t, offsetof
#include <cstdint>
#include <cmath>
#include <stdlib.h> // rand

#define two_pi 6.2831853071795864f

#define COUT_V2(X) "(" << (X).x << ", " << (X).y << ")"

// low quality
float rand01 ()
{
    return float(rand()) / float(RAND_MAX);
}

struct v4
{
    float x, y, z, w;
};

struct v2
{
    float x, y;
};

inline v2 operator- (v2 a, v2 b)
{
    return {a.x - b.x, a.y - b.y};
}

inline v2 operator/ (v2 v, float k)
{
    return {v.x / k, v.y / k};
}

inline float dot(v2 a, v2 b)
{
    return a.x * b.x + a.y * b.y;
}

inline float length (v2 v)
{
    return std::sqrt(v.x * v.x + v.y * v.y);
}

struct line_attr
{
    v4 p0;
    v4 p1;
    v4 c0;
    v4 c1;
    v2 tf0;
    v2 tf1;
};

struct vertex_pt
{
    float p[4];
    uint8_t t[2];
};

static const char* vert = R"(
#version 330
layout(location=0) in vec4 attr_position;
layout(location=1) in vec2 attr_texcoord;
out vec2 texcoord;
void main()
{
    gl_Position = attr_position;
    texcoord = attr_texcoord;
}
)";

static const char* frag = R"(
#version 330
uniform sampler2D tex0;
in vec2 texcoord;
layout(location=0) out vec4 frag_color;
void main()
{
    frag_color = texture(tex0, texcoord);
}
)";

static const char* vs = R"(
#version 330
layout(location=0) in vec4 p0;
layout(location=1) in vec4 p1;
layout(location=2) in vec4 c0;
layout(location=3) in vec4 c1;
layout(location=4) in vec2 tf0;
layout(location=5) in vec2 tf1;
uniform vec2 resolution;
out vec4 color;
out float thickness;
out float feather;
out vec2 texcoord;
//out float transition;
void main()
{
    float width = resolution.x;
    float height = resolution.y;
    float ratio = width / height;

    float t0 = tf0.x / height;
    float t1 = tf1.x / height;

    float tns0 = tf0.y / height;
    float tns1 = tf1.y / height;

    vec4 r0 = p0;
    vec4 r1 = p1;
    vec4 color0 = c0;
    vec4 color1 = c1;

    r0.x *= ratio;
    r1.x *= ratio;
    vec2 dir = normalize(r1.xy - r0.xy);
    vec2 normal = vec2(dir.y, -dir.x);
    float t = max(t0, t1) + max(tns0, tns1) * 2.0;
    vec4 d = vec4(t * dir, 0.0, 0.0);
    vec4 n = vec4(t * normal, 0.0, 0.0);
    vec4 pos;

    if (gl_VertexID == 0)
    {
        pos = r0 - n - d;
        texcoord = vec2(-1.0, -1.0);
    }
    else if (gl_VertexID == 1)
    {
        pos = r0 + n - d;
        texcoord = vec2(-1.0, 1.0);
    }
    else if (gl_VertexID == 2)
    {
        pos = r0 - n;
        texcoord = vec2(0.0, -1.0);
    }
    else if (gl_VertexID == 3)
    {
        pos = r0 + n;
        texcoord = vec2(0.0, 1.0);
    }
    else if (gl_VertexID == 4)
    {
        pos = r1 - n;
        texcoord = vec2(0.0, -1.0);
    }
    else if (gl_VertexID == 5)
    {
        pos = r1 + n;
        texcoord = vec2(0.0, 1.0);
    }
    else if (gl_VertexID == 6)
    {
        pos = r1 + d - n;
        texcoord = vec2(1.0, -1.0);
    }
    else // gl_VertexID == 7
    {
        pos = r1 + d + n;
        texcoord = vec2(1.0, 1.0);
    }

    if (gl_VertexID < 4)
    {
        color = color0;
        thickness = t0 / t;
        feather = (t0 + tns0) / t;
    }
    else
    {
        color = color1;
        thickness = t1 / t;
        feather = (t1 + tns1) / t;
    }

    pos.x /= ratio;
    gl_Position = pos;
}
)";

static const char* fr = R"(
#version 330
in vec4 color;
in float thickness;
in float feather;
in vec2 texcoord;
layout(location=0) out vec4 frag_color;

// https://github.com/mattdesl/webgl-wireframes/blob/gh-pages/lib/wire.frag
float aastep (float threshold, float dist)
{
  float afwidth = fwidth(dist);
  return smoothstep(threshold - afwidth, threshold + afwidth, dist);
}

void main()
{
    float l = length(texcoord);

    // float x = 1.0 - smoothstep(thickness, feather, l);

    // l = clamp(l, thickness, feather) - thickness;
    // float x = 1.0 - l / (feather - thickness);

    float x = 1.0 - aastep(thickness, l);

    frag_color = vec4(color.rgb, color.a * x);
}
)";

static const char* quad_vert = R"(
#version 330
out vec2 texcoord;
void main()
{
    if (gl_VertexID == 0)
    {
        gl_Position = vec4(-1.0, -1.0, 0.0, 1.0);
        texcoord = vec2(0.0, 0.0);
    }
    else if (gl_VertexID == 1)
    {
        gl_Position = vec4(3.0, -1.0, 0.0, 1.0);
        texcoord = vec2(2.0, 0.0);
    }
    else
    {
        gl_Position = vec4(-1.0, 3.0, 0.0, 1.0);
        texcoord = vec2(0.0, 2.0);
    }
}
)";

static const char* quad_frag = R"(
#version 330
uniform sampler2D tex0;
in vec2 texcoord;
layout(location=0) out vec4 frag_color;
void main()
{
    frag_color = texture(tex0, texcoord);
}
)";