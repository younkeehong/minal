#!/bin/bash

(
cd build/ninja/release
ninja
)

BUILD_RESULT=$?
if [ ${BUILD_RESULT} == 0 ]; then
    ./minal_test
fi
