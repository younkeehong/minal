@echo off
PUSHD .
md build
cd build
md ninja
cd ninja
md release
cd release
cmake -G "Ninja" -DCMAKE_BUILD_TYPE=Release ../../..
POPD
