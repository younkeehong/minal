#include "minal/minal.hpp"
#include "util.hpp"

using namespace std;

struct app_t
{
    float previous = 0.1f;
    bool adding_points = false;
    window* win;
    audio* dac;
};

static void osc_callback (osc_recv_msg* msg, void* shared)
{
    auto* s = (app_t*)shared;
    const char* addr = get_addr(msg);
    if (match_pattern(msg, "/test"))
    {
        int a = pop_i32(msg);
        float x = pop_f32(msg);
        cout << "pattern matched, " << a << " & " << x << endl;
    }
    cout << "\"" << addr << "\" received" << endl;
}

static void key_callback (GLFWwindow* w, int key, int code, int action, int mods)
{
    if (is_mod_key(key)) return;

    auto* app = (app_t*)get_user_pointer(w);

    size_t m = (size_t)(mods);
    size_t alt = m & GLFW_MOD_ALT;
    size_t ctrl = m & GLFW_MOD_CONTROL;
    size_t shift = m & GLFW_MOD_SHIFT;

    if (key == GLFW_KEY_Q && ctrl)
    {
        if (action == GLFW_PRESS)
        {
            set_should_close(w);
        }
        return;
    }

    if (key == GLFW_KEY_F && ctrl)
    {
        if (action == GLFW_PRESS)
        {
            if (is_fullscreen(w))
            {
                end_fullscreen(app->win);
            }
            else
            {
                go_fullscreen(app->win);
            }
        }
        return;
    }

    if (using_gui()) return;

    switch (action)
    {
        case GLFW_PRESS:
            if (key == GLFW_KEY_SPACE)
            {
                cout << "checking" << endl;
            }
            break;
        case GLFW_REPEAT:
            break;
        case GLFW_RELEASE:
            break;
    }
}

void mouse_callback (GLFWwindow* w, int button, int action, int mods)
{
    if (using_gui()) return;

    size_t m = (size_t)(mods);
    size_t alt = m & GLFW_MOD_ALT;
    size_t ctrl = m & GLFW_MOD_CONTROL;
    size_t shift = m & GLFW_MOD_SHIFT;

    auto* app = (app_t*)get_user_pointer(w);

    switch (action)
    {
        case GLFW_PRESS:
            app->adding_points = true;
            break;
        case GLFW_RELEASE:
            app->adding_points = false;
            break;
    }
}

static int audio_process (void* out, void* in, unsigned num_frames,
                          double time, unsigned status, void* data)
{
    if (status == RTAUDIO_OUTPUT_UNDERFLOW) cout << "output underflow" << endl;
    else if (status == RTAUDIO_INPUT_OVERFLOW) cout << "input overflow" << endl;

    float* buffer = (float*)out;
    app_t* a = (app_t*)data;
    float& t = a->previous;
    float sr = float(a->dac->sampling_rate);

    for (unsigned i = 0; i < num_frames; i += 1) {
        t += 440.0f / sr;
        if (t > 1) t -= 1;
        float s = sin(two_pi * t);
        buffer[2 * i + 0] = s;
        buffer[2 * i + 1] = s;
    }

    return 0;
}

int main ()
{
    app_t app;

    osc_server osc_serv;
    start_osc_server(&osc_serv, 6789, osc_callback, &app);

    osc_sender osc_send;
    bind_sender(&osc_send, "localhost", 12000);

    osc_send_msg msg;
    reset_msg(&msg, "/test");
    push_i32(&msg, 3);
    push_f32(&msg, 3.0f);
    bool sent = send(&osc_send, &msg);

    sound_sample wav = load_wav("beat.wav");
    cout << "channels: " << wav.channels << ", sample rate: " << wav.sample_rate
         << ", frame_count: " << wav.frame_count << endl;
    cout << "total length: " << (double(wav.frame_count) / double(wav.sample_rate)) << " sec" << endl;

    toml_io toml1 = parse_toml("test.toml");
    double toml_d = getf64(&toml1, "my_float");
    int64_t toml_i = geti64(&toml1, "my_int");
    bool toml_b = getb(&toml1, "my_bool");
    cout << toml_d << ", " << toml_i << ", " << toml_b << endl;

    cout << "copying" << endl;
    toml_io toml2 = toml1;
    double toml2_d = getf64(&toml2, "my_float");
    int64_t toml2_i = geti64(&toml2, "my_int");
    bool toml2_b = getb(&toml2, "my_bool");
    cout << toml2_d << ", " << toml2_i << ", " << toml2_b << endl;

    json_io json1 = parse_json("test.json");
    double json_d = getf64(&json1, "my_float");
    int64_t json_i = geti64(&json1, "my_int");
    bool json_b = getb(&json1, "my_bool");
    cout << json_d << ", " << json_i << ", " << json_b << endl;

    cout << "copying" << endl;
    json_io json2 = json1;
    double json2_d = getf64(&json2, "my_float");
    int64_t json2_i = geti64(&json2, "my_int");
    bool json2_b = getb(&json2, "my_bool");
    cout << json2_d << ", " << json2_i << ", " << json2_b << endl;

    window win;

    cout << "f for fullscreen, empty or anything else for window: " << flush;
    string window_type;
    getline(cin, window_type);
    if (window_type == "f")
    {
        int n = print_monitors();
        cout << "type monitor idx, empty for primary: " << flush;
        string monitor_pick;
        getline(cin, monitor_pick);
        try
        {
            int picked = stoi(monitor_pick);
            if (picked > n - 1) picked = n - 1;
            cout << "picked: " << picked << endl;
            open_fullscreen(&win, picked);
        }
        catch (std::invalid_argument)
        {
            open_fullscreen(&win);
        }
    }
    else
    {
        open_window_ms(&win, 720, 640, 4);
    }
    if (!win.glfw_win) return 1;
    app.win = &win;

    set_user_pointer(&win, &app);
    set_key_callback(&win, key_callback);
    set_mouse_callback(&win, mouse_callback);

    audio dac;
    open_audio_stream(&dac, audio_process, &app);
    if (!is_audio_stream_open(&dac)) return 1;
    app.dac = &dac;

    init_imgui(&win);

    shader_manager shaders;
    texture_manager textures;
    fbo_manager fbos;

    shader s = create_shader(&shaders, vert, frag);
    uniform loc_tex0 = uniform_loc(s, "tex0");

    shader sandline = create_shader(&shaders, vs, fr);
    uniform resolution = uniform_loc(sandline, "resolution");

    vao_info info;

    // p0
    info.attr[0].buffer = 0;
    info.attr[0].dimension = 4;
    info.attr[0].type = GL_FLOAT;
    info.attr[0].stride = sizeof(line_attr);
    info.attr[0].offset = offsetof(line_attr, p0);
    info.attr[0].per_instance = true;

    // p1
    info.attr[1].buffer = 0;
    info.attr[1].dimension = 4;
    info.attr[1].type = GL_FLOAT;
    info.attr[1].stride = sizeof(line_attr);
    info.attr[1].offset = offsetof(line_attr, p1);
    info.attr[1].per_instance = true;

    // c0
    info.attr[2].buffer = 0;
    info.attr[2].dimension = 4;
    info.attr[2].type = GL_FLOAT;
    info.attr[2].stride = sizeof(line_attr);
    info.attr[2].offset = offsetof(line_attr, c0);
    info.attr[2].per_instance = true;

    // c1
    info.attr[3].buffer = 0;
    info.attr[3].dimension = 4;
    info.attr[3].type = GL_FLOAT;
    info.attr[3].stride = sizeof(line_attr);
    info.attr[3].offset = offsetof(line_attr, c1);
    info.attr[3].per_instance = true;

    // tf0
    info.attr[4].buffer = 0;
    info.attr[4].dimension = 2;
    info.attr[4].type = GL_FLOAT;
    info.attr[4].stride = sizeof(line_attr);
    info.attr[4].offset = offsetof(line_attr, tf0);
    info.attr[4].per_instance = true;

    // tf1
    info.attr[5].buffer = 0;
    info.attr[5].dimension = 2;
    info.attr[5].type = GL_FLOAT;
    info.attr[5].stride = sizeof(line_attr);
    info.attr[5].offset = offsetof(line_attr, tf1);
    info.attr[5].per_instance = true;

    info.use_indices = true;

    vao lines = create_vao(info);

    uint32_t idx[] = {
        0, 1, 3,
        0, 3, 2,
        2, 3, 5,
        2, 5, 4,
        4, 5, 7,
        4, 7, 6
    };

    v4 p0 = {0.3f, 0.3f, 0.0f, 1.0f};
    v4 p1 = {0.6f, 0.3f, 0.0f, 1.0f};
    v4 p2 = {0.6f, 0.6f, 0.0f, 1.0f};
    v4 p3 = {0.8f, 0.55f, 0.0f, 1.0f};
    v4 p4 = {0.6f, 0.9f, 0.0f, 1.0f};
    v4 p5 = {0.7f, 0.2f, 0.0f, 1.0f};

    v4 c0 = {1.0f, 0.0f, 0.0f, 0.0f};
    v4 c1 = {0.0f, 1.0f, 0.0f, 0.5f};
    v4 c2 = {0.0f, 1.0f, 1.0f, 1.0f};

    v2 tf0 = {0.0f, 3.0f};
    v2 tf1 = {15.0f, 3.0f};
    v2 tf2 = {8.0f, 3.0f};

    line_attr lines_data[] = {
        {p0, p1, c0, c1, tf0, tf1},
        {p1, p2, c1, c2, tf1, tf2},
        {p2, p5, c2, c1, tf2, tf1},
        {p3, p4, c1, c0, tf1, tf0}
    };

    buffer_data(lines, 0, 4 * sizeof(line_attr), lines_data);
    index_data(lines, 18 * sizeof(uint32_t), idx);

    std::vector<line_attr> line_buffer;
    v4 prev_p;
    v4 prev_c {rand01(), rand01(), rand01(), rand01()};
    v2 prev_tf {rand01() * 10.0f, rand01() * 5.0f + 3.0f};
    v2 prev_d {0.0f, 0.0f};

    // glfw give mouse coord origin at top left
    // convert it to [-1:1] * [-1:1] viewport coord
    auto to_viewport_coord = [&] (float win_x, float win_y) -> v2
    {
        return {
            2.0f * (win_x / app.win->width) - 1.0f,
            -2.0f * (win_y / app.win->height) + 1.0f
        };
    };

    auto normalize = [] (v2 v, float* l_out) -> v2
    {
        float l = length(v);
        if (l < 0.000001f)
        {
            *l_out = 0.0f;
            return {0.0f, 0.0f};
        }
        else
        {
            *l_out = l;
            return v / l;
        }
    };

    float thickness = 0.5f;
    float feather = 2.5f;

    auto add_line = [&] (v2 p)
    {
        v2 new_p = {1.0f * (p.x / app.win->width) - 1.0f, 2.0f * (p.y / app.win->height) - 1.0f};
        new_p.y = -new_p.y; // flip cuz oF glfw mouse coord is top left origin

        float step;
        v2 d = normalize(new_p - v2{prev_p.x, prev_p.y}, &step);

        // minimum distance
        if (step < 0.005f)
        {
            return;
        }

        // only add if enough change in dir or dist
        if (dot(d, prev_d) < 0.9f || step > 0.01f)
        {
            v4 p1 = {new_p.x, new_p.y, 0.0f, 1.0f};
            // v4 c1 = {rand01(), rand01(), rand01(), rand01()};
            // v2 tf1 = {rand01() * 10.0f, rand01() * 5.0f + 3.0f};

            v4 c1 = {1.0f, 1.0f, 1.0f, 1.0f};
            v2 tf1 = {thickness, feather};

            line_buffer.push_back(
                {prev_p, p1, prev_c, c1, prev_tf, tf1}
            );
            prev_p = p1;
            prev_c = c1;
            prev_tf = tf1;
            prev_d = d;
        }
    };

    auto add_line2 = [&] (v2 p0, v2 p1)
    {
        v4 c = {1.0f, 1.0f, 1.0f, 1.0f};
        v2 tf = {thickness, feather};
        line_buffer.push_back(
            {v4{p0.x, p0.y, 0.0f, 1.0f},
             v4{p1.x, p1.y, 0.0f, 1.0f},
             c, c, tf, tf}
        );
    };

    auto add_line_rand = [&] ()
    {
        v4 p0 = {2.0f * rand01() - 1.0f, 2.0f * rand01() - 1.0f, 0.0f, 1.0f};
        v4 p1 = {2.0f * rand01() - 1.0f, 2.0f * rand01() - 1.0f, 0.0f, 1.0f};
        v4 c0 = {rand01(), rand01(), rand01(), rand01()};
        v4 c1 = {rand01(), rand01(), rand01(), rand01()};
        v2 tf0 = {rand01() * 10.0f, rand01() * 5.0f + 3.0f};
        v2 tf1 = {rand01() * 10.0f, rand01() * 5.0f + 3.0f};
        line_buffer.push_back(
            {p0, p1, c0, c1, tf0, tf1}
        );
    };

    texture tex1 = create_texture(&textures, win.framebuffer_width, win.framebuffer_height);

    fbo_info fi1;
    fi1.color[0] = tex1;
    fi1.width = win.framebuffer_width;
    fi1.height = win.framebuffer_height;
    fbo fbo1 = create_fbo(&fbos, fi1);

    vao_info quad_info;
    vao viewport_quad = create_vao(quad_info);

    shader quad_shader = create_shader(&shaders, quad_vert, quad_frag);
    uniform loc_tex1 = uniform_loc(quad_shader, "tex0");

    GL_ERROR("setup");

    vao_info vi;

    vi.attr[0].buffer = 0;
    vi.attr[0].dimension = 4;
    vi.attr[0].type = GL_FLOAT;
    vi.attr[0].stride = sizeof(vertex_pt);
    vi.attr[0].offset = 0;
    vi.attr[0].normalize = false;

    vi.attr[1].buffer = 0;
    vi.attr[1].dimension = 2;
    vi.attr[1].type = GL_UNSIGNED_BYTE;
    vi.attr[1].stride = vi.attr[0].stride;
    vi.attr[1].offset = offsetof(vertex_pt, t);
    vi.attr[1].normalize = true;

    vao v = create_vao(vi);

    image sprite {256, 256};
    for (int y = 0; y < 256; y += 1)
    {
        for (int x = 0; x < 256; x += 1)
        {
            int i = x + 256 * y;
            sprite.data[4 * i + 0] = uint8_t(x);
            sprite.data[4 * i + 1] = 127;
            sprite.data[4 * i + 2] = uint8_t(y);
            sprite.data[4 * i + 3] = 255;
        }
    }
    save_image("sprite.png", sprite.width, sprite.height, sprite.data);

    image img = load_image("test.png");
    if (!(img.data))
    {
        cout << "???" << endl;
    }

    // tex to sample
    tex_info ti;
    ti.data = img.data;
    texture tex = create_texture(&textures, img.width, img.height, ti);

    // to draw to
    texture tex0 = create_texture(&textures, 256, 256);
    fbo_info fi;
    fi.color[0] = tex0;
    fi.width = 256;
    fi.height = 256;
    fbo fbo0 = create_fbo(&fbos, fi);

    float background[4] = {0, 0, 0, 1};
    float t = 0;

    vertex_pt vertices[] = {
        {-0.9f, -0.5f, 0.0f, 1.0f, 0, 0},
        {0.1f, -0.5f, 0.0f, 1.0f, 255, 0},
        {-0.5f, 0.5f, 0.0f, 1.0f, 0, 255},

        {0.1f, -0.5f, 0.0f, 1.0f, 255, 0},
        {-0.5f, 0.5f, 0.0f, 1.0f, 0, 255},
        {0.4f, 0.5f, 0.0f, 1.0f, 255, 255},
    };

    //ImVec4 bgcol = ImVec4(0.45f, 0.55f, 0.60f, 1.00f);
    float clear2[4] = {1.0f, 0.0f, 0.0f, 1.0f};

    tex_ms_info tmi;
    tmi.samples = 4;
    texture_ms texms = create_texture_ms(256, 256, tmi);
    fbo_ms_info fmi;
    fmi.color[0] = texms;
    fmi.width = 256;
    fmi.height = 256;
    fbo_ms fboms = create_fbo_ms(fmi);
    GL_ERROR("MS");

    bool wireframe = false;
    float mx, my;
    float pmx, pmy;

    start_audio_stream(&dac);
    cursor_pos(&win, &mx, &my);
    pmx = mx;
    pmy = my;

    GL_ERROR("before start");

    bool use_ms = false;

    while (!should_close(&win)) {
        update_size(&win);
        cursor_pos(&win, &mx, &my);

        t += 0.0001f;

        imgui_begin_frame();
        begin_control();

        float fps = get_fps();
        gui_text("%.3f ms/frame (%.1f FPS)", 1000.0f / fps, fps);

        static float val = 0.0f;
        sliderf("test f", &val);
        sliderf("thickness", &thickness, 0.0f, 16.0f);
        sliderf("feather", &feather, 0.0f, 16.0f);

        newline();

        static int choice = 0;
        const char* options[] = {
            "option0",
            "option1",
            "option2"
        };
        bool radio_pressed = radioh(options, 3, &choice);
        if (radio_pressed)
        {
            cout << "radio pressed" << endl;
        }

        toggle("wireframe", &wireframe);
        toggle("adding points", &(app.adding_points));
        toggle("ms fbo", &(use_ms));

        hline();

        static int selected = 0;
        const char* items[] = { "a", "b", "c" };
        combo("combo", &selected, items, 3);

        gui_image(tex0.tex, 128, 128);
        gui_image(tex1.tex, 128, 128);

        end_control();

        begin_inspector();
        sliderf("inspector val", &val);
        end_inspector();

        imgui_end_frame();

/*
        if (app.adding_points)
        {
            add_line({mx, my});
        }
        else
        {
            prev_p = {2.0f * (mx / app.win->width) - 1.0f, 2.0f * (my / app.win->height) - 1.0f, 0.0f, 1.0f};
            prev_p.y = -prev_p.y;

            //line_buffer.clear();
            //for (int i = 0; i < 50; i += 1)
            //{
            //    add_line_rand();
            //}
        }
*/

        if (t > 1.0f) t -= 1.0f;
        line_buffer.clear();
        float pi = 3.1415926535f;
        v2 p0 = {cos(2.0f * pi * t), sin(2.0f * pi * t)};
        v2 p1 = {0.75f * cos(2.0f * 2.0f * pi * t), 0.5f * sin(2.0f * 2.0f * pi * t)};
        add_line2(p0, p1);

        int n = int(line_buffer.size());
        buffer_data(lines, 0, n * sizeof(line_attr), line_buffer.data());

        buffer_data(v, 0, 6 * sizeof(vertex_pt), vertices);

        GL_ERROR("before render");
        {
            if (use_ms)
            {
                glBindFramebuffer(GL_FRAMEBUFFER, fboms.fb);
                glViewport(0, 0, 256, 256);
            }
            else
            {
                bind_fbo(fbo0);
            }
            clear_color(clear2);

            use_program(s);
            uniform1i(loc_tex0, 0);
            depth_test(false);
            bind_vao(v);
            bind_texture2d(tex, 0);
            draw_triangles(6);
            unbind_texture2d(0);
        }

        if (use_ms)
        {
            GL_ERROR("before blit");
            glBindFramebuffer(GL_READ_FRAMEBUFFER, fboms.fb);
            glBindFramebuffer(GL_DRAW_FRAMEBUFFER, fbo0.fb);
            glBlitFramebuffer(0, 0, 256, 256,
                              0, 0, 256, 256, GL_COLOR_BUFFER_BIT, GL_NEAREST);
            glBindFramebuffer(GL_READ_FRAMEBUFFER, 0);
            glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
            GL_ERROR("after blit");
        }

        {
            bind_fbo(fbo1);
            float bg[] = {0.0f, 0.0f, 0.0f, 0.0f};
            clear_color(bg);

            use_program(sandline);
            uniform2f(resolution, float(win.framebuffer_width), float(win.framebuffer_height));
            bind_vao(lines);
            bind_idx(lines);
            depth_test(false);
            depth_mask(false);
            blending(true);
            blend_eq(GL_MAX);
            blend_func(GL_ONE, GL_ONE);
            if (wireframe) glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
            if (wireframe) blending(false);
            draw_triangles_idx_instance(18, n);
            blending(false);
            if (wireframe) glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        }

        {
            default_framebuffer(&win);
            clear_color(background);

            use_program(s);
            uniform1i(loc_tex0, 0);
            depth_test(false);
            bind_vao(v);
            bind_texture2d(tex0, 0);
            draw_triangles(6);
            unbind_texture2d(0);

            use_program(quad_shader);
            uniform1i(loc_tex1, 0);
            bind_texture2d(tex1, 0);
            bind_vao(viewport_quad);
            depth_test(false);
            blending(true);
            if (wireframe) blending(false);
            blend_eq(GL_FUNC_ADD);
            blend_func(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
            draw_triangles(3);
            unbind_texture2d(0);
            blending(false);

            GL_ERROR("after default2");

            imgui_draw();
        }

        GL_ERROR("after imgui");

        pmx = mx;
        pmy = my;

        swap_buffers(&win);
        poll_events();
    }

    delete_shaders(&shaders);
    delete_textures(&textures);
    delete_fbos(&fbos);

    GL_ERROR("before deleting");
    delete_texture_ms(&texms);
    delete_fbo_ms(&fboms);
    GL_ERROR("deleting");

    delete_vao(&v);

    quit_imgui();
    quit_osc_server(&osc_serv);
    close_audio_stream(&dac);
    close_window(&win);
}
