#define GLFW_INCLUDE_NONE
#include "GLFW/glfw3.h"

#ifdef APIENTRY
#undef APIENTRY
#endif

#include "RtAudio.h"

#include "oscpkt.hh"
#ifndef _MSC_VER
#include <unistd.h> // for ::close
#endif
#include "udp.hh"

#ifdef APIENTRY
#undef APIENTRY
#endif

// include minal header with MINAL_INTERNAL defined
// and after glfw & rtaudio headers so that
// the header does not use forward declarations but the original code
#define MINAL_INTERNAL
#include "minal/minal.hpp"

#include "imgui.h"
#include "imgui_impl_glfw.h"
#include "imgui_impl_opengl3.h"

#include "cpptoml.h"

#include "nlohmann/json.hpp"
using json = nlohmann::json;

#include <mutex>
#include <cstring> // memcpy
#include <fstream>

using namespace std;

// http://insanecoding.blogspot.com/2011/11/how-to-read-in-file-in-c.html
std::string file_to_string (const char* name)
{
    std::ifstream in(name, std::ios::in | std::ios::binary);

     if (in)
     {
        std::string contents;
        in.seekg(0, std::ios::end);
        contents.resize(in.tellg());

        in.seekg(0, std::ios::beg);
        in.read(&contents[0], contents.size());
        in.close();
        return contents;
     }

     else
     {
        std::cout << "Failed to load file: " << name << std::endl;
        return "";
     }
}

void print_if (const char* label, const char* err)
{
    if (err && err[0])
    {
        std::cout << label << ": " << err << std::endl;
    }
}

static void glfw_error_callback (int code, const char* description)
{
  cout << "glfw error [" << code << "]: " << description << endl;
}

struct window_hint
{
    int multisample = 0;
    bool decorated = true;
    int monitor = -1;
};

static void open_window (window* win, bool fullscreen, int width, int height, window_hint hint)
{
    if (!glfwInit())
    {
        cout << "create_window: glfwInit failed" << endl;
        return;
    }

    glfwSetErrorCallback(glfw_error_callback);

    glfwDefaultWindowHints();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GLFW_TRUE);
    glfwWindowHint(GLFW_AUTO_ICONIFY, GLFW_FALSE);
    glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);
    glfwWindowHint(GLFW_SAMPLES, hint.multisample);
    glfwWindowHint(GLFW_DECORATED, hint.decorated? GLFW_TRUE : GLFW_FALSE);

    GLFWwindow* w = nullptr;
    if (fullscreen)
    {
        GLFWmonitor* monitor;
        if (hint.monitor > -1)
        {
            int count;
            GLFWmonitor** monitors = glfwGetMonitors(&count);
            monitor = monitors[hint.monitor];
        }
        else
        {
            monitor = glfwGetPrimaryMonitor();
        }
        const GLFWvidmode* mode = glfwGetVideoMode(monitor);
        w = glfwCreateWindow(mode->width, mode->height, "", monitor, nullptr);
    }
    else
    {
        w = glfwCreateWindow(width, height, "", nullptr, nullptr);
    }

    if (!w)
    {
        cout << "create_window: glfwCreateWindow failed" << endl;
        return;
    }

    glfwMakeContextCurrent(w);
    glfwSwapInterval(1);

    if (!gladLoadGL())
    {
        glfwDestroyWindow(w);
        w = nullptr;
        cout << "create_window: gladLoadGL failed" << endl;
        return;
    }

    win->glfw_win = w;
    glfwGetWindowSize(win->glfw_win, &(win->width), &(win->height));
    glfwGetFramebufferSize(win->glfw_win, &(win->framebuffer_width), &(win->framebuffer_height));
    if (fullscreen)
    {
        // arbitrary size when starting as fullscreen
        win->windowed_width = 320;
        win->windowed_height = 200;
    }
    else
    {
        win->windowed_width = win->width;
        win->windowed_height = win->height;
    }
}

int print_monitors ()
{
    if (!glfwInit())
    {
        cout << "print_monitors: glfwInit failed" << endl;
        return 0;
    }

    int count;
    GLFWmonitor** monitors = glfwGetMonitors(&count);
    for (int i = 0; i < count; i += 1)
    {
        const GLFWvidmode* m = glfwGetVideoMode(monitors[i]);
        cout << "monitor " << i << ": " << monitors[i] << '\n'
             << "\tsize: " << m->width << ", " << m->height << '\n'
             << "\trate: " << m->refreshRate << '\n'
             << "\tbits: " << m->redBits << ", " << m->greenBits << ", " << m->blueBits
             << "\n\n";
    }
    GLFWmonitor* primary = glfwGetPrimaryMonitor();
    cout << "primary: " << primary << '\n' <<  endl;
    return count;
}

void open_fullscreen (window* win)
{
    window_hint hint;
    open_window(win, true, 0, 0, hint);
}

void open_fullscreen (window* win, int idx)
{
    window_hint hint;
    hint.monitor = idx;
    open_window(win, true, 0, 0, hint);
}

void open_window (window* win, int width, int height)
{
    window_hint hint;
    open_window(win, false, width, height, hint);
}

void open_window_ms (window* win, int width, int height, int samples)
{
    window_hint hint;
    hint.multisample = samples;
    open_window(win, false, width, height, hint);
}

void close_window (window* win)
{
    if (win->glfw_win)
    {
        glfwDestroyWindow(win->glfw_win);
        win->glfw_win = nullptr;
    }
}

void go_fullscreen (window* win)
{
    GLFWmonitor* p = glfwGetPrimaryMonitor();
    const GLFWvidmode* m = glfwGetVideoMode(p);
    glfwSetWindowMonitor(win->glfw_win, p, 0, 0, m->width, m->height, m->refreshRate);
    glfwMakeContextCurrent(win->glfw_win);
    glfwSwapInterval(1);
    glfwGetWindowSize(win->glfw_win, &(win->width), &(win->height));
    glfwGetFramebufferSize(win->glfw_win, &(win->framebuffer_width), &(win->framebuffer_height));
}

void end_fullscreen (window* win)
{
    // (50, 50) so window decoration is not hidden
    glfwSetWindowMonitor(win->glfw_win, nullptr,
                         50, 50, win->windowed_width, win->windowed_height, 0);
    glfwMakeContextCurrent(win->glfw_win);
    glfwSwapInterval(1);
    glfwGetWindowSize(win->glfw_win, &(win->width), &(win->height));
    glfwGetFramebufferSize(win->glfw_win, &(win->framebuffer_width), &(win->framebuffer_height));
}

bool is_fullscreen (GLFWwindow* w)
{
    GLFWmonitor* m = glfwGetWindowMonitor(w);
    if (m) return true;
    else return false;
}

void update_size (window* w)
{
    glfwGetWindowSize(w->glfw_win, &(w->width), &(w->height));
    glfwGetFramebufferSize(w->glfw_win, &(w->framebuffer_width), &(w->framebuffer_height));
}

bool should_close (window* win)
{
    return glfwWindowShouldClose(win->glfw_win);
}

void swap_buffers (window* win)
{
    glfwSwapBuffers(win->glfw_win);
}

void poll_events ()
{
    glfwPollEvents();
}

void set_key_callback (window* win, GLFWkeyfun func)
{
    glfwSetKeyCallback(win->glfw_win, func);
}

void set_mouse_callback (window* win, GLFWmousebuttonfun func)
{
    glfwSetMouseButtonCallback(win->glfw_win, func);
}

void set_user_pointer (window* win, void* data)
{
    glfwSetWindowUserPointer(win->glfw_win, data);
}

void* get_user_pointer (GLFWwindow* w)
{
    return glfwGetWindowUserPointer(w);
}

void set_should_close (GLFWwindow* w)
{
    glfwSetWindowShouldClose(w, GLFW_TRUE);
}

bool is_mod_key (int key)
{
    if (key == GLFW_KEY_LEFT_SHIFT) return true;
    if (key == GLFW_KEY_LEFT_CONTROL) return true;
    if (key == GLFW_KEY_LEFT_ALT) return true;
    if (key == GLFW_KEY_LEFT_SUPER) return true;
    if (key == GLFW_KEY_RIGHT_SHIFT) return true;
    if (key == GLFW_KEY_RIGHT_CONTROL) return true;
    if (key == GLFW_KEY_RIGHT_ALT) return true;
    if (key == GLFW_KEY_RIGHT_SUPER) return true;

    return false;
}

void cursor_pos (window* w, float* x, float* y)
{
    double xpos, ypos;
    glfwGetCursorPos(w->glfw_win, &xpos, &ypos);
    *x = float(xpos);
    *y = float(ypos);
}

window::~window ()
{
    if (glfw_win)
    {
        glfwDestroyWindow(glfw_win);
        glfw_win = nullptr;
    }
    glfwTerminate();
}

void rtaudio_error_callback (RtAudioError::Type type, const std::string& error_text )
{
    const char* type_str = "";
    switch (type)
    {
        case RtAudioError::WARNING:           type_str = "WARNING"; break;
        case RtAudioError::DEBUG_WARNING:     type_str = "DEBUG_WARNING"; break;
        case RtAudioError::UNSPECIFIED:       type_str = "UNSPECIFIED"; break;
        case RtAudioError::NO_DEVICES_FOUND:  type_str = "NO_DEVICES_FOUND"; break;
        case RtAudioError::INVALID_DEVICE:    type_str = "INVALID_DEVICE"; break;
        case RtAudioError::MEMORY_ERROR:      type_str = "MEMEORY_ERROR"; break;
        case RtAudioError::INVALID_PARAMETER: type_str = "INVALID_PARAMETER"; break;
        case RtAudioError::INVALID_USE:       type_str = "INVALID_USE"; break;
        case RtAudioError::DRIVER_ERROR:      type_str = "DRIVER_ERROR"; break;
        case RtAudioError::SYSTEM_ERROR:      type_str = "SYSTEM_ERROR"; break;
        case RtAudioError::THREAD_ERROR:      type_str = "THREAD_ERROR"; break;
    }
    cout << "RtAudio error: " << type_str << ", " << error_text << endl;
}

void open_audio_stream (audio* dac, RtAudioCallback callback, void* data)
{
    if (!dac->rtaudio) dac->rtaudio = new RtAudio;
    unsigned device = dac->rtaudio->getDefaultOutputDevice();
    RtAudio::DeviceInfo info = dac->rtaudio->getDeviceInfo(device);
    if (info.probed)
    {
        RtAudio::StreamParameters param;
        param.deviceId = device;
        param.nChannels = info.outputChannels;
        unsigned sample_rate = info.preferredSampleRate;
        unsigned buffer_size = 512;
        RtAudio::StreamOptions options;
        try
        {
            dac->rtaudio->openStream(&param, nullptr, RTAUDIO_FLOAT32, sample_rate,
                                    &buffer_size, callback, data, &options,
                                    rtaudio_error_callback);
            dac->num_buffers = options.numberOfBuffers;
            dac->sampling_rate = sample_rate;
            dac->device_id = device;
            dac->channels = param.nChannels;
            dac->buffer_size = buffer_size;
        }
        catch (RtAudioError& e)
        {
            cout << "RtAudio failed to open/start stream: " << e.getMessage() << endl;
        }
    }
    else
    {
        cout << "open_audio_stream error: Default device not probed" << endl;
    }
}

bool is_audio_stream_open (audio* dac)
{
    return dac->rtaudio->isStreamOpen();
}

void start_audio_stream (audio* dac)
{
    dac->rtaudio->startStream();
}

void close_audio_stream (audio* dac)
{
    if (dac->rtaudio)
    {
        if (dac->rtaudio->isStreamRunning())
        {
            try
            {
                dac->rtaudio->stopStream();
            }
            catch (RtAudioError& e)
            {
                cout << e.getMessage() << endl;
            }
        }
        if (dac->rtaudio->isStreamOpen())
        {
            dac->rtaudio->closeStream();
        }
    }
}

audio::~audio ()
{
    if (rtaudio)
    {
        if (rtaudio->isStreamRunning())
        {
            try
            {
                rtaudio->stopStream();
            }
            catch (RtAudioError& e)
            {
                cout << e.getMessage() << endl;
            }
        }
        if (rtaudio->isStreamOpen())
        {
            rtaudio->closeStream();
        }
        delete rtaudio;
    }
}

shader create_shader (const char* vert, const char* frag)
{
    shader s;

    GLuint v = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(v, 1, &vert, nullptr);
    glCompileShader(v);
    GLint vert_compile_status = GL_FALSE;
    glGetShaderiv(v, GL_COMPILE_STATUS, &vert_compile_status);
    if (vert_compile_status != GL_TRUE) {
        GLint log_len;
        glGetShaderiv(v, GL_INFO_LOG_LENGTH, &log_len);
        char* buf = new char[log_len];
        glGetShaderInfoLog(v, log_len, nullptr, buf);
        cout << "failed compiling vertex shader:\n" << buf << endl;
        delete[] buf;
        glDeleteShader(v);
        return s; // return empty shader
    }

    GLuint f = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(f, 1, &frag, nullptr);
    glCompileShader(f);
    GLint frag_compile_status = GL_FALSE;
    glGetShaderiv(f, GL_COMPILE_STATUS, &frag_compile_status);
    if (frag_compile_status != GL_TRUE) {
        GLint log_len;
        glGetShaderiv(f, GL_INFO_LOG_LENGTH, &log_len);
        char* buf = new char[log_len];
        glGetShaderInfoLog(f, log_len, nullptr, buf);
        cout << "failed compiling fragment shader:\n" << buf << endl;
        delete[] buf;
        glDeleteShader(v);
        glDeleteShader(f);
        return s; // return empty shader
    }

    GLuint p = glCreateProgram();
    glAttachShader(p, v);
    glAttachShader(p, f);
    glLinkProgram(p);
    GLint program_link_status = GL_FALSE;
    glGetProgramiv(p, GL_LINK_STATUS, &program_link_status);
    if (program_link_status != GL_TRUE) {
        GLint log_len;
        glGetProgramiv(p, GL_INFO_LOG_LENGTH, &log_len);
        char* buf = new char[log_len];
        glGetProgramInfoLog(p, log_len, nullptr, buf);
        cout << "failed linking shader program:\n" << buf << endl;
        delete[] buf;
        glDeleteShader(v);
        glDeleteShader(f);
        glDeleteProgram(p);
        return s;
    }

    glDetachShader(p, v);
    glDetachShader(p, f);
    glDeleteShader(v);
    glDeleteShader(f);

    s.program = p;
    return s;
}

void delete_shader (shader* s)
{
    glDeleteProgram(s->program);
    s->program = 0;
}

vao create_vao (vao_info info)
{
    vao v;
    glGenVertexArrays( 1, &(v.va) );
    glBindVertexArray(v.va);

    for (int i = 0; i < VAO_ATTR_NUM; i += 1)
    {
        attr_info& a = info.attr[i];
        if (a.buffer >= 0)
        {
            if (a.buffer >= VAO_BUFFER_NUM)
            {
                cout << "requested buffer index larger than supported number of buffers for vao" << endl;
                delete_vao(&v);
                return v;
            }

            GLuint& b = v.buffer[a.buffer];
            if (b == 0)
            {
                glGenBuffers(1, &b);
            }
            glBindBuffer(GL_ARRAY_BUFFER, b);
            glEnableVertexAttribArray(i);
            GLint dimension = (GLint)a.dimension;
            GLenum type = a.type;
            GLboolean normalize = a.normalize? GL_TRUE : GL_FALSE;
            GLsizei stride = (GLsizei)a.stride;
            ptrdiff_t offset = (ptrdiff_t)a.offset;
            glVertexAttribPointer(i, dimension, type, normalize,
                                  stride, (const GLvoid*)offset);

            if (a.per_instance)
            {
                glVertexAttribDivisor((GLuint)i, 1);
            }
        }
    }

    if (info.use_indices)
    {
        glGenBuffers(1, &(v.indices));
    }
    return v;
}

void delete_vao (vao* v)
{
    glDeleteVertexArrays(1, &(v->va) );
    v->va = 0;
    for (int i = 0; i < 4; i += 1)
    {
        if (v->buffer[i] != 0)
        {
            glDeleteBuffers(1, &(v->buffer[i]) );
            v->buffer[i] = 0;
        }
    }
    if (v->indices != 0)
    {
        glDeleteBuffers(1, &(v->indices) );
        v->indices = 0;
    }
}

texture create_texture (int width, int height, tex_info info)
{
    GLsizei w = (GLsizei)width;
    GLsizei h = (GLsizei)height;
    texture t;
    glGenTextures(1, &(t.tex) );

    // 47 as temporary binding point, trying not to overwrite other bindings
    // ref: GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS, "In GL 3.3, this was 48; in 4.3, it is 96"
    // https://www.khronos.org/opengl/wiki/Shader#Resource_limitations
    glActiveTexture(GL_TEXTURE0 + 47);
    glBindTexture(GL_TEXTURE_2D, t.tex);

    GLint level = 0; // no mipmap by default
    GLint border = 0; // should always be 0

    glTexImage2D(GL_TEXTURE_2D, level, info.format, w, h, border,
                 info.data_format, info.data_type, info.data);

    // to make texture complete, disable mipmap and set non mipmap filters
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, 0);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 0);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, info.filter);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, info.filter);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, info.wrap);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, info.wrap);

    return t;
}

void delete_texture (texture* t)
{
    glDeleteTextures(1, &(t->tex) );
    t->tex = 0;
}

texture_ms create_texture_ms (int width, int height, tex_ms_info info)
{
    GLsizei w = (GLsizei)width;
    GLsizei h = (GLsizei)height;
    texture_ms t;
    glGenTextures(1, &(t.tex) );

    // 47 as temporary binding point, trying not to overwrite other bindings
    // ref: GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS, "In GL 3.3, this was 48; in 4.3, it is 96"
    // https://www.khronos.org/opengl/wiki/Shader#Resource_limitations
    glActiveTexture(GL_TEXTURE0 + 47);
    glBindTexture(GL_TEXTURE_2D_MULTISAMPLE, t.tex);

    glTexImage2DMultisample(GL_TEXTURE_2D_MULTISAMPLE, info.samples, info.format, w, h, GL_TRUE);
    t.samples = info.samples;
    return t;
}

void delete_texture_ms (texture_ms* t)
{
    glDeleteTextures(1, &(t->tex) );
    t->tex = 0;
}

fbo create_fbo (fbo_info info)
{
    fbo f;
    glGenFramebuffers(1, &(f.fb) );
    glBindFramebuffer(GL_FRAMEBUFFER, f.fb);
    for (int i = 0; i < FBO_COLOR_NUM; i += 1)
    {
        if (info.color[i].tex != 0)
        {
            glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + i, info.color[i].tex, 0);
        }
    }
    if (info.depth.tex != 0)
    {
        // attach depth tex
        glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, info.depth.tex, 0);
    }
    else
    {
        glGenRenderbuffers(1, &(f.depth));
        glBindRenderbuffer(GL_RENDERBUFFER, f.depth);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, info.width, info.height);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, f.depth);
    }
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    f.width = info.width;
    f.height = info.height;
    return f;
}

void delete_fbo (fbo* f)
{
    glDeleteFramebuffers(1, &(f->fb) );
    if (f->depth != 0)
    {
        glDeleteRenderbuffers(1, &(f->depth) );
    }
}

fbo_ms create_fbo_ms (fbo_ms_info info)
{
    fbo_ms f;
    glGenFramebuffers(1, &(f.fb) );
    glBindFramebuffer(GL_FRAMEBUFFER, f.fb);
    GLsizei samples = -1;
    for (int i = 0; i < FBO_COLOR_NUM; i += 1)
    {
        if (info.color[i].tex != 0)
        {
            glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + i,
                                   GL_TEXTURE_2D_MULTISAMPLE,
                                   info.color[i].tex, 0);
            if (samples < 0)
            {
                samples = info.color[i].samples;
            }
            else
            {
                if (samples != info.color[i].samples)
                {
                    cout << "create_fbo_ms: multisample number different between attached textures" << endl;
                }
            }
        }
    }
    if (info.depth.tex != 0)
    {
        // attach depth tex
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT,
                               GL_TEXTURE_2D_MULTISAMPLE,
                               info.depth.tex, 0);
        if (samples != info.depth.samples)
        {
            cout << "create_fbo_ms: multisample number different between attached color textures and depth texture" << endl;
        }
    }
    else
    {
        glGenRenderbuffers(1, &(f.depth));
        glBindRenderbuffer(GL_RENDERBUFFER, f.depth);
        glRenderbufferStorageMultisample(GL_RENDERBUFFER, samples, GL_DEPTH_COMPONENT24, info.width, info.height);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, f.depth);
    }

    GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
    if (status != GL_FRAMEBUFFER_COMPLETE)
    {
        cout << "something wrong with ms fbo" << endl;
    }

    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    f.width = info.width;
    f.height = info.height;
    return f;
}

void delete_fbo_ms (fbo_ms* f)
{
    glDeleteFramebuffers(1, &(f->fb) );
    if (f->depth != 0)
    {
        glDeleteRenderbuffers(1, &(f->depth) );
    }
}

shader create_shader (shader_manager* m, const char* vert, const char* frag)
{
    m->shaders.emplace_back();
    shader& s = m->shaders.back();
    s = create_shader(vert, frag);
    return s;
}

void delete_shaders (shader_manager* m)
{
    for (shader& s : m->shaders)
    {
        if (s.program != 0)
        {
            delete_shader(&s);
        }
    }
}

texture create_texture (texture_manager* m, int width, int height, tex_info ti)
{
    m->textures.emplace_back();
    texture& t = m->textures.back();
    t = create_texture(width, height, ti);
    return t;
}

texture create_texture (texture_manager* m, int width, int height)
{
    tex_info ti;
    return create_texture(m, width, height, ti);
}

void delete_textures (texture_manager* m)
{
    for (texture& t : m->textures)
    {
        if (t.tex != 0)
        {
            delete_texture(&t);
        }
    }
}

fbo create_fbo (fbo_manager* m, fbo_info fi)
{
    m->fbos.emplace_back();
    fbo& f = m->fbos.back();
    f = create_fbo(fi);
    return f;
}

void delete_fbos (fbo_manager* m)
{
    for (fbo& f : m->fbos)
    {
        if (f.fb != 0)
        {
            delete_fbo(&f);
        }
    }
}

vao create_vao (vao_manager* m, vao_info vi)
{
    m->vaos.emplace_back();
    vao& v = m->vaos.back();
    v = create_vao(vi);
    return v;
}

void delete_vaos (vao_manager* m)
{
    for (vao& v : m->vaos)
    {
        if (v.va != 0)
        {
            delete_vao(&v);
        }
    }
}

void init_imgui (window* win)
{
    const char* glsl_version = "#version 330";

    // Setup Dear ImGui context
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    // ImGuiIO& io = ImGui::GetIO(); (void)io;
    //io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;  // Enable Keyboard Controls
    //io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;   // Enable Gamepad Controls

    // Setup Platform/Renderer bindings
    ImGui_ImplGlfw_InitForOpenGL(win->glfw_win, true);
    ImGui_ImplOpenGL3_Init(glsl_version);

    ImGui::StyleColorsDark();
    //ImGui::StyleColorsClassic();
}

void imgui_begin_frame ()
{
    ImGui_ImplOpenGL3_NewFrame();
    ImGui_ImplGlfw_NewFrame();
    ImGui::NewFrame();
}

void imgui_end_frame ()
{
    ImGui::Render();
}

void begin_inspector ()
{
    GLFWwindow* win = glfwGetCurrentContext();
    int w, h;
    glfwGetWindowSize(win, &w, &h);

    ImGuiWindowFlags flags = 0;
    flags |= ImGuiWindowFlags_NoTitleBar;
    flags |= ImGuiWindowFlags_NoMove;
    flags |= ImGuiWindowFlags_AlwaysAutoResize;
    flags |= ImGuiWindowFlags_NoInputs;
    flags |= ImGuiWindowFlags_NoBackground;
    ImGui::SetNextWindowPos(ImVec2(float(w), 0.0f), 0, ImVec2(1.0f, 0.0f));
    ImGui::Begin("inspector window", nullptr, flags);
    ImGui::PushItemWidth(100);
}

void end_inspector ()
{
    ImGui::End();
}

void begin_control ()
{
    ImGuiWindowFlags flags = 0;
    flags |= ImGuiWindowFlags_NoTitleBar;
    flags |= ImGuiWindowFlags_NoMove;
    flags |= ImGuiWindowFlags_AlwaysAutoResize;
    flags |= ImGuiWindowFlags_NoBackground;
    ImGui::SetNextWindowPos(ImVec2(0.0f, 0.0f), 0, ImVec2(0.0f, 0.0f));
    ImGui::Begin("control window", nullptr, flags);
    ImGui::PushItemWidth(200);
}

void end_control ()
{
    ImGui::End();
}

void imgui_draw ()
{
    ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
}

void quit_imgui ()
{
    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplGlfw_Shutdown();
    ImGui::DestroyContext();
}

bool button (const char* label)
{
    return ImGui::Button(label);
}

bool toggle (const char* label, bool* val)
{
    return ImGui::Checkbox(label, val);
}

bool sliderf (const char* label, float* val, float mini, float maxi)
{
    return ImGui::SliderFloat(label, val, mini, maxi);
}

bool slideri (const char* label, int* val, int minimum, int maximum)
{
    return ImGui::SliderInt(label, val, minimum, maximum);
}

bool using_gui ()
{
    if (!ImGui::GetCurrentContext()) return false;

    auto& io = ImGui::GetIO();
    bool capture = io.WantCaptureMouse | io.WantCaptureKeyboard
                                       | io.WantTextInput;
    return capture;
}

void gui_image (GLuint id, float width, float height)
{
    // flip upside down
    ImGui::Image((void*)(intptr_t)(id), ImVec2(width, height), ImVec2(0,1), ImVec2(1,0));
}

void gui_text (const char* fmt, ...)
{
    // rewrite of ImGui::Text
    va_list args;
    va_start(args, fmt);
    ImGui::TextV(fmt, args);
    va_end(args);
}

void hline () { ImGui::Separator(); }
void newline () { ImGui::NewLine(); }
void spacing () { ImGui::Spacing(); }
void same_line () { ImGui::SameLine(); }
void gui_item_width (float w) { ImGui::PushItemWidth(w); }

bool radio (const char* label, int* idx, int val)
{
    return ImGui::RadioButton(label, idx, val);
}

bool radioh (const char** labels, int num_options, int* val)
{
    bool pressed = false;
    for (int i = 0; i < num_options; i += 1)
    {
        if (ImGui::RadioButton(labels[i], val, i))
        {
            pressed = true;
        }
        if (i != num_options - 1) same_line();
    }
    return pressed;
}

bool radiov (const char** labels, int num_options, int* val)
{
    bool pressed = false;
    for (int i = 0; i < num_options; i += 1)
    {
        if (ImGui::RadioButton(labels[i], val, i))
        {
            pressed = true;
        }
    }
    return pressed;
}

bool combo (const char* label, int* current_item, const char** items, int items_count)
{
    return ImGui::Combo(label, current_item, items, items_count);
}

float get_fps ()
{
    return ImGui::GetIO().Framerate;
}

struct osc_recv_msg
{
    oscpkt::Message* msg;;
    oscpkt::Message::ArgReader* reader;
};

static void server_func (int port, osc_recv_callback_t callback, void* shared, atomic<bool>* quit_flag)
{
    oscpkt::UdpSocket sock;
    sock.bindTo(port);

    if (sock.isOk())
    {
        oscpkt::PacketReader pr;
        while (sock.isOk())
        {
            if (quit_flag->load()) break;

            if (sock.receiveNextPacket(50 /* timeout, in ms */))
            {
                pr.init(sock.packetData(), sock.packetSize());
                oscpkt::Message *msg;
                while (pr.isOk() && (msg = pr.popMessage()) != 0)
                {
                    oscpkt::Message::ArgReader reader {*msg, oscpkt::OK_NO_ERROR};
                    osc_recv_msg r {msg, &reader};
                    callback(&r, shared);
                }
            }
        }
    }
    else cerr << "Error opening port " << port << ": " << sock.errorMessage() << "\n";
}

void start_osc_server (osc_server* osc, int port, osc_recv_callback_t cb, void* shared)
{
    if (osc->thread_handle.joinable())
    {
        osc->quit_flag.store(true);
        osc->thread_handle.join();
    }
    osc->quit_flag.store(false);
    osc->thread_handle = std::thread(server_func, port, cb, shared, &(osc->quit_flag));
}

void quit_osc_server (osc_server* osc)
{
    osc->quit_flag.store(true);
    if (osc->thread_handle.joinable())
    {
        osc->thread_handle.join();
    }
}

const char* get_addr (osc_recv_msg* r)
{
    return r->msg->addressPattern().c_str();
}

bool match_pattern (osc_recv_msg* r, const char* pattern)
{
    return oscpkt::fullPatternMatch(r->msg->addressPattern().c_str(), pattern);
}

int32_t pop_i32 (osc_recv_msg* r)
{
    int32_t i;
    r->reader->popInt32(i);
    return i;
}

float pop_f32 (osc_recv_msg* r)
{
    float f;
    r->reader->popFloat(f);
    return f;
}

osc_sender::osc_sender ()
{
    socket = new oscpkt::UdpSocket;
    writer = new oscpkt::PacketWriter;
}

osc_sender::~osc_sender ()
{
    delete socket;
    delete writer;
}

osc_send_msg::osc_send_msg ()
{
    msg = new oscpkt::Message;
}

osc_send_msg::~osc_send_msg ()
{
    delete msg;
}

void bind_sender (osc_sender* s, const char* addr, int port)
{
    s->socket->connectTo(addr, port);
    if (!(s->socket->isOk()))
    {
        cout << "Error connection to port " << port << ": " << s->socket->errorMessage() << endl;
    }
}

void reset_msg (osc_send_msg* m, const char* addr)
{
    m->msg->init(addr);
}

void push_i32 (osc_send_msg* m, int32_t i)
{
    m->msg->pushInt32(i);
}

void push_f32 (osc_send_msg* m, float f)
{
    m->msg->pushFloat(f);
}

bool send (osc_sender* s, osc_send_msg* m)
{
    if (s->socket->isOk())
    {
        s->writer->init().addMessage(*(m->msg)); // copies the msg
        bool ok = s->socket->sendPacket(s->writer->packetData(), s->writer->packetSize());
        return ok;
    }
    return false;
}

bool send (osc_sender* s, void* data, size_t size)
{
    bool ok = s->socket->sendPacket(data, size);
    return ok;
}

struct toml_impl
{
    std::shared_ptr<cpptoml::table> root;
};

toml_io::toml_io ()
{
    impl = new toml_impl;
}

toml_io::toml_io (const toml_io& other)
{
    impl = new toml_impl;
    if (other.impl->root)
    {
        impl->root = dynamic_pointer_cast<cpptoml::table, cpptoml::base>(other.impl->root->clone());
    }
}

toml_io::toml_io (toml_io&& other) noexcept
{
    impl = other.impl;
    other.impl = nullptr;
}

toml_io& toml_io::operator= (const toml_io& other)
{
    if (other.impl->root)
    {
        impl->root = dynamic_pointer_cast<cpptoml::table, cpptoml::base>(other.impl->root->clone());
    }
    return *this;
}

toml_io& toml_io::operator= (toml_io&& other) noexcept
{
    delete impl;
    impl = other.impl;
    other.impl = nullptr;
    return *this;
}

toml_io::~toml_io ()
{
    delete impl;
}

toml_io parse_toml (const char* name)
{
    toml_io t;
    try
    {
        t.impl->root = cpptoml::parse_file(name);
    }
    catch (const cpptoml::parse_exception& e)
    {
        cout << "failed to parse " << name << "(" << e.what() << ")" << endl;
    }
    return t;
}

template<typename T>
T get (toml_io* io, const std::string& key)
{
    if (!(io->impl->root))
    {
        cout << "no toml table loaded" << endl;
        return T{};
    }

    auto val = io->impl->root->get_as<T>(key);
    if (!val)
    {
        cout << "toml: key '" << key << "' not found" << endl;
        return T{};
    }
    return *val;
}

double getf64 (toml_io* t, const char* key)
{
    return get<double>(t, key);
}

int64_t geti64 (toml_io* t, const char* key)
{
    return get<int64_t>(t, key);
}

std::string gets(toml_io* t, const char* key)
{
    return get<std::string>(t, key);
}

bool getb (toml_io* t, const char* key)
{
    return get<bool>(t, key);
}

struct json_impl
{
    json j;
};

json_io::json_io ()
{
    impl = new json_impl;
}

json_io::json_io (const json_io& other)
{
    impl = new json_impl;
    impl->j = other.impl->j;
}

json_io::json_io (json_io&& other) noexcept
{
    impl = other.impl;
    other.impl = nullptr;
}

json_io& json_io::operator= (const json_io& other)
{
    if (!impl) impl = new json_impl;
    impl->j = other.impl->j;
    return *this;
}

json_io& json_io::operator= (json_io&& other) noexcept
{
    delete impl;
    impl = other.impl;
    other.impl = nullptr;
    return *this;
}

json_io::~json_io ()
{
    delete impl;
}

json_io parse_json (const char* name)
{
    json_io j;
    std::ifstream i {name};
    i >> j.impl->j;
    return j;
}

double getf64 (json_io* j, const char* key)
{
    return (j->impl->j)[key].get<double>();
}

int64_t geti64 (json_io* j, const char* key)
{
    return (j->impl->j)[key].get<int64_t>();
}

std::string gets(json_io* j, const char* key)
{
    return (j->impl->j)[key].get<std::string>();
}

bool getb (json_io* j, const char* key)
{
    return (j->impl->j)[key].get<bool>();
}

extern "C" {
unsigned char *stbi_load(char const *filename, int *x, int *y, int *channels_in_file, int desired_channels);
void stbi_image_free(void *retval_from_stbi_load);

int stbi_write_png(char const *filename, int w, int h, int comp, const void *data, int stride_in_bytes);
}

image::image (int w, int h)
{
    data = new uint8_t[4 * w * h];
    width = w;
    height = h;
}

image::image (const image& other)
{
    int size = 4 * other.width * other.height;
    data = (uint8_t*)malloc(size);
    memcpy(data, other.data, size);
    width = other.width;
    height = other.height;
}

image::image (image&& other) noexcept
{
    data = other.data;
    other.data = nullptr;
    width = other.width;
    height = other.height;
}

image& image::operator= (const image& other)
{
    delete[] data;
    int size = 4 * other.width * other.height;
    data = (uint8_t*)malloc(size);
    memcpy(data, other.data, size);
    width = other.width;
    height = other.height;
    return *this;
}

image& image::operator= (image&& other) noexcept
{
    delete[] data;
    data = other.data;
    other.data = nullptr;
    return *this;
}

image::~image ()
{
    delete[] data;
}

image load_image (const char* filename)
{
    image img;
    int w, h, n;
    // n will contain number of channels originally in image file
    // last parameter '4': force 4 channels
    unsigned char *data = stbi_load(filename, &w, &h, &n, 4);
    if (!data) return img; // return empty object
    int size = 4 * w * h;
    img.data = (uint8_t*)malloc(size);
    memcpy(img.data, data, w * h * 4);
    stbi_image_free(data);
    img.width = w;
    img.height = h;
    return img;
}

void save_image (const char* filename, int w, int h, uint8_t* data)
{
    int result = stbi_write_png(filename, w, h, 4, data, w * 4);
    if (result == 0)
    {
        cout << "failed to write image: " << filename << endl;
    }
}

extern "C"
{
float* drwav_open_file_and_read_pcm_frames_f32(const char* filename, unsigned int* channels, unsigned int* sampleRate, uint64_t* totalFrameCount);
void drwav_free(void* pDataReturnedByOpenAndRead);
}

sound_sample::sound_sample (const sound_sample& other)
{
    uint64_t size = other.frame_count * other.channels;
    samples = (float*)malloc(sizeof(float) * size);
    memcpy(samples, other.samples, sizeof(float) * size);
    channels = other.channels;
    sample_rate = other.sample_rate;
    frame_count = other.frame_count;
}

sound_sample::sound_sample (sound_sample&& other) noexcept
{
    samples = other.samples;
    other.samples = nullptr;
    channels = other.channels;
    sample_rate = other.sample_rate;
    frame_count = other.frame_count;
}

sound_sample& sound_sample::operator= (const sound_sample& other)
{
    if (samples)
    {
        drwav_free(samples);
    }
    uint64_t size = other.frame_count * other.channels;
    samples = (float*)malloc(sizeof(float) * size);
    memcpy(samples, other.samples, sizeof(float) * size);
    channels = other.channels;
    sample_rate = other.sample_rate;
    frame_count = other.frame_count;
    return *this;
}

sound_sample& sound_sample::operator= (sound_sample&& other) noexcept
{
    if (samples)
    {
        drwav_free(samples);
    }
    samples = other.samples;
    other.samples = nullptr;
    channels = other.channels;
    sample_rate = other.sample_rate;
    frame_count = other.frame_count;
    return *this;
}

sound_sample::~sound_sample ()
{
    if (samples)
    {
        drwav_free(samples);
    }
}

sound_sample load_wav (const char* filename)
{
    sound_sample wav;
    wav.samples = drwav_open_file_and_read_pcm_frames_f32(filename, &(wav.channels),
                                                          &(wav.sample_rate), &(wav.frame_count));
    if (!(wav.samples))
    {
        cout << "failed to open " << filename << endl;
    }

    return wav;
}

void sleep_thread (int ms)
{
    this_thread::sleep_for(std::chrono::milliseconds(ms));
}

/*
* PCG Random Number Generation for C.
*
* Copyright 2014 Melissa O'Neill <oneill@pcg-random.org>
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*
* For additional information about the PCG random number generation scheme,
* including its license and other licensing options, visit
*
*     http://www.pcg-random.org
*/

static void pcg_advance(pcg_state* pcg)
{
    pcg->state = pcg->state * 6364136223846793005ULL + pcg->inc;
}

pcg_state pcg_seed(uint64_t initstate, uint64_t initseq)
{
    pcg_state pcg;
    pcg.state = 0U;
    pcg.inc = (initseq << 1u) | 1u;
    pcg_advance(&pcg);
    pcg.state += initstate;
    pcg_advance(&pcg);
    return pcg;
}

uint32_t pcg_run(pcg_state* pcg)
{
    uint64_t oldstate = pcg->state;
    pcg_advance(pcg);
    uint32_t xorshifted = (uint32_t)( ((oldstate >> 18u) ^ oldstate) >> 27u );
    uint32_t rot = oldstate >> 59u;
    return (xorshifted >> rot) | (xorshifted << ((~rot + 1u) & 31u));
}

float rand01(pcg_state* pcg)
{
    return float(pcg_run(pcg)) / float(UINT32_MAX);
}